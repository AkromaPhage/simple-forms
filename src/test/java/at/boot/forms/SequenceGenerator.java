package at.boot.forms;

import org.junit.jupiter.api.Test;

import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SequenceGenerator {

    @Test
    void shouldGenerateSequenceOfInputs() {
        int number = 100;
        String[] params = {"d", "q", "k"};
        Random random = new Random();

        String output = IntStream.range(0, number)
                .mapToObj(i -> params[random.nextInt(3)] + (random.nextInt(999) + 1))
                .collect(Collectors.joining(" "));

        System.out.println(output);

    }

}
