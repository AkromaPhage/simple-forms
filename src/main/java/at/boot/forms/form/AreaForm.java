package at.boot.forms.form;

public interface AreaForm {

    double getArea();

    long getLength();

}
