package at.boot.forms.form;

public class Square implements VolumeForm {

    private final Long length;

    public Square(Long length) {
        this.length = length;
    }

    @Override
    public double getArea() {
        return length * length;
    }

    @Override
    public double getVolume() {
        return getArea() * length;
    }

    @Override
    public long getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "Square{" +
                "length=" + length +
                ", area=" + getArea() +
                '}';
    }
}
