package at.boot.forms.form;

public interface VolumeForm extends AreaForm {

    double getVolume();

}
