package at.boot.forms.form;

public class Triangle implements AreaForm {

    private final Long length;

    public Triangle(Long length) {
        this.length = length;
    }

    @Override
    public double getArea() {
        double height = (length / 2) * Math.sqrt(3);
        return (length * height) / 2;
    }

    @Override
    public long getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "Triangle{" +
                "length=" + length +
                ", area=" + getArea() +
                '}';
    }
}
