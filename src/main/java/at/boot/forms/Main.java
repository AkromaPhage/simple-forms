package at.boot.forms;

import at.boot.forms.form.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<AreaForm> list = readForms(args);

        Collections.reverse(list);
        for (AreaForm areaForm : list) {
            System.out.println(areaForm);
        }

        System.out.println("===============================================");
        System.out.println("                    VOLUME                     ");
        System.out.println("===============================================");

        Collections.reverse(list);

        for (AreaForm form : list) {
            if (form instanceof VolumeForm) {
                System.out.printf("%s[length=%s, volume=%s]", form.getClass().getSimpleName(), form.getLength(), ((VolumeForm) form).getVolume());
                System.out.println();
            }
        }
    }

    private static List<AreaForm> readForms(String[] args) {
        List<AreaForm> list = new ArrayList<>();
        for (String argument : args) {
            String firstDigit = argument.substring(0, 1);
            Long value = Long.valueOf(argument.substring(1));
            switch (firstDigit.toLowerCase()) {
                case "d":
                    list.add(new Triangle(value));
                    break;
                case "k":
                    list.add(new Circle(value));
                    break;
                case "q":
                    list.add(new Square(value));
                    break;
            }
        }
        return list;
    }

}
